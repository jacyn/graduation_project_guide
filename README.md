# 本科毕业设计指南

毕业设计是由学生独立完成一个课题，并最终以学位论文的形式呈现。通过本科毕业设计将本科阶段所学的知识、技能串联起来，并通过本科毕设项目的牵引，学会如何做到目标分析、计划、工作分配、编程、实验、总结、汇报等多种能力，从而为后续的工作、研究生阶段的工作开展奠定能力基础。

![logo](images/senior_design.jpg)

通过毕业设计需要达到的[《知识、能力、思维的目标》](Targets.md)，可以好好琢磨一下如何达到所列的能力，如何评估自己是否达到这样的能力。


## 1. 研究课题

根据自己的兴趣、背景知识，选择下面列的一个研究课题：

### 1.1 机器学习
* [《无人机目标识别与跟踪》](https://gitee.com/pi-lab/pi-det)：在学习物体识别的基础上，实现在线增量式学习
* [《在线学习与增量学习》](https://gitee.com/pi-lab/research_online_learning)： 研究如何实现模型训练与推理融合一体，从而应对新的场景与变化。
* [《差异检测》](https://gitee.com/pi-lab/research_change_detection)：研究如何有效对比不同时刻的图像，判断语义层面的差异和变化
* [《基于知识的深度强化学习》](https://gitee.com/pi-lab/research_knowledge_dqn)：研究基于知识表达的深度强化学习，实现智能程度更高的智能控制方法


### 1.2 计算机视觉
* [《语义定位与地图》](https://gitee.com/pi-lab/research_semantic_localization)：研究如何通过构建认知地图的方式，实现空间定位等。
* [《视觉SLAM》](https://gitee.com/pi-lab/research_deep_SLAM): 研究如何融合传统的SLAM与深度学习SLAM，从而模拟人类的环境感知。
* [《无人机与地面机器人的协同环境感知与控制》](https://gitee.com/pi-lab/research_air_gound_collaboration)：研究如何通过无人机生成环境地图，给地面机器人做导航控制


### 1.3 无人机/机器人控制
* [《机器人跟踪》](https://gitee.com/pi-lab/tracking-robot): 学习ROS，控制地面机器人完成集群机器人的控制，例如跟踪特定目标、保持队形
* [《无人机自主降落 - 控制与仿真程序》](https://gitee.com/pi-lab/uav-autolanding): 视觉解算无人机相对地面的标志物的位置、姿态，来控制无人机的降落，实现全自主、高精度的降落
* [《无人机自主导航》](https://gitee.com/pi-lab/research_autonomous_navigation)：研究视觉SLAM和无人机控制，实现无人机在卫星导航失效的情况下的自主定位与控制
* [《无人机飞行控制器》](https://gitee.com/pi-lab/research_flight_controller)：研究无人机飞控、地面站、集群控制。



## 2. 如何具体操作？

请参考 [《毕业设计示例项目》](https://gitee.com/ice-shadow/research_environment_perception)， 并按照导师给大家发的毕业任务书，将自己的制定的研究计划等整理到自己的研究项目，创建上传[Gitee](https://gitee.com/)项目，方便大家和导师的协同；另外在整个过程将资料、代码、PPT、毕业论文等都通过Git管理，这样不容易丢失。

具体的操作流程：
1. 大家将自己的研究项目在[Gitee](https://gitee.com/)上创建，[或者fork这个项目到自己的项目](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/HowToForkClone.md)
2. 把导师加入到各自的研究项目，具体的说明参考[《Gitee增加用户》](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/gitee_addmember.md)
3. 在理解课题的基础的上，制定更详细的毕业任务书，可以参考[《毕业设计任务书 - 无人飞行器的语义地图》](samples/assignment_semantic_map.md)
4. 根据自己的研究目标，制定自己的[《研究计划》](samples/research_planning_simp.md)，主要包括：研究目标，研究思路，关键技术，研究计划，参考资料等。如果对自己有更高要求，可以制定[《更详细的研究计划》](samples/research_planning.md)。
5. 开展基础知识学习，课题研究。
6. 找一篇和自己研究最贴切的一篇英文论文，对该论文进行翻译，在翻译过程学习：这个研究方向的现状、进展情况、主要的方法、问题和挑战、如何复现等等。
7. 定期开会汇报进展工作，每次准备的报告材料可以用Markdown编写，或者整理成PPT。
8. 进展到2个月左右的时候，开始准备并进行开题报告，可以参考[《开题报告示例》](doc/thesis_proposal_sample.pdf)、[《开题报告PPT示例》](doc/thesis_proposal_ppt_sample.pdf)。主要包括的内容：选题背景和目的、国内外研究现状、课题研究目标、研究内容、研究方法及关键技术、论文所遇到的困难和问题、拟采取的解决措施及预期达到的目标、论文进度安排、参考文献等。
9. [毕业论文的撰写，检查](https://gitee.com/pi-lab/pilab_research_fields/tree/master/tips/learn_writting/thesis)，在撰写过程可以参考[《毕业论文参考》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/learn_writting/thesis/sample/thesis_sample.pdf)。完成之后，对自己的论文进行[格式、内容检查](doc/thesis_checklist.docx)
10. 为了更好的展示自己的研究成果，大家把自己的研究成果素材收集好，编辑一个1-3分钟的视频，通过这个视频让其他人更好的理解自己做的工作。具体可以参考借鉴[《示例成果视频》](https://www.bilibili.com/video/BV1a5411j7kH) 。
11. 大家准备答辩的PPT并进行答辩，将自己的研究目的、内容、结果、成果通过PPT等展现给评委老师。



## 3. 基础知识学习


### 3.1 毕设的前奏
研究不仅仅是时间的付出，更多的是学习、琢磨研究的方法等，因此需要先学习一下：
* [《学习成功之道》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/Tao_for_Success)
* [《如何做研究》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/HowToResearch.md)
* [《如何学习 - 研究的心得与方法》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/README.md)


### 3.2 基础知识学习
大家评估自己的能力，并制定有针对性的基础能力学习：
* 如果编程能力比较弱，可以好好学习编程： [《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)
* [《如何做研究》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/HowToResearch.md)
* [《如何写毕业论文》](https://gitee.com/pi-lab/pilab_research_fields/tree/master/tips/learn_writting/thesis)


### 3.3 机器学习方面的研究
* 如果做机器学习方面的研究，可以自学[《机器学习》](https://gitee.com/pi-lab/machinelearning_notebook)，在线教程视频：[《B站 - 机器学习》](https://www.bilibili.com/video/BV1oZ4y1N7ei/)
* [《90分钟学会Python》](https://gitee.com/pi-lab/machinelearning_notebook/tree/master/0_python)


### 3.4 计算机视觉方面的研究
如果做视觉方面的研究，可以学习如下的课程：
* [《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)
* [《一步一步学ROS》](https://gitee.com/pi-lab/learn_ros)
* [《无人机飞行控制器》](https://gitee.com/pi-lab/research_flight_controller)

### 3.5 编程、技能

* [编程代码参考、技巧集合](https://gitee.com/pi-lab/code_cook): 可以在这个代码、技巧集合中找到某项功能的示例，从而加快自己代码的编写

* 飞行器智能感知与控制实验室-培训教程与作业

    - [《飞行器智能感知与控制实验室-暑期培训教程》](https://gitee.com/pi-lab/SummerCamp)
    - [《飞行器智能感知与控制实验室-暑期培训作业》](https://gitee.com/pi-lab/SummerCampHomework)

* [研究、科研方法，教程](https://gitee.com/pi-lab/pilab_research_fields)

    

## 4. 参考资料

### 4.1 毕业设计参考：
* 可以参考[《毕业设计参考、示例项目》](https://gitee.com/ice-shadow/research_environment_perception)
* 建议大家使用[《Latex》](https://gitee.com/pi-lab/SummerCamp/tree/master/tool/latex)来撰写自己的毕业论文，[《西北工业大学 - 学士论文LaTex模版与示例》](https://gitee.com/pi-lab/template_bachelor)
* [《如何做研究》](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/HowToResearch.md)
* [《如何写毕业论文》](https://gitee.com/pi-lab/pilab_research_fields/tree/master/tips/learn_writting/thesis)

### 4.2 工具的使用教程等：
由于需要使用Markdown和Git，因此大家可以静下心好好学习一下这两个工具
* [Markdown](https://gitee.com/pi-lab/learn_programming/6_tools/markdown)
* [Git](https://gitee.com/pi-lab/learn_programming/6_tools/git)

其他的参考资料：
* [Code Cook - 编程参考代码，技巧集合](https://gitee.com/pi-lab/code_cook)
* [Linux](https://gitee.com/pi-lab/learn_programming/6_tools/linux)
* [CMake](https://gitee.com/pi-lab/learn_programming/6_tools/cmake)


