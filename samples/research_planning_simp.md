# 如何编写研究计划？

学习和研究过程中，做好自己的计划编排对后续研究顺利开展起到关键的作用。在研究的前期，自己搜集资料、阅读、思考，从而基本了解研究对象、特点、发展趋势等，从而为后续的研究工作开展起到积极的作用；在研究过程中，随着对研究对象、方法的深入掌握，会对研究有新的认识与理解，所以需要不断修正。

不论你的研究领域是什么，你选择了什么样的方法，所有的研究计划必须解决以下问题：你打算完成什么，为什么你要做这件事，以及你打算怎样去完成它。大家在开展研究工作之前，需要把下面所列的问题、工作等认真完成。按照下面所列的提纲，把各项内容写出，并且在研究开展过程，及时修改并上传到git仓库，从而方便后续研究开展不断思考与反思。如果把所有的内容都想清楚并列出来太难，可以把已经想清楚的写出来，在研究过程随着认识的深入而逐步完善。




## 1. 研究背景与意义

**研究背景与意义主要描述清楚为什么要做这个研究，目前国内外已经做到了什么程度，目前仍有什么难题需要解决。**

### 1.1 研究意义
* 主要讲述所研究课题的大目标，以及为什么要研究此课题。
* 可以通过回答以下问题来定位研究意义
     * 目前的研究有那些，存在什么问题
     * 本研究尝试解决什么问题？研究是否具有创新性？
     * 研究与别人的方法有什么不同？
     * 研究是否前沿？
     * 研究是否解决某项实际问题？

### 1.2 现状
* 基于自己的课题，查找相关文献进行总结
* 可以从以下方面进行总结
     * 已有工作都用的什么方法
     * 现有的方法有什么好处和劣势
     * 是否有地方可以改进

## 2. 研究内容
### 2.1 研究目标
* 与1.1中研究课题大目标不同的是，这里的目标要具体，在结题时可达到

### 2.2 研究内容
* 一般写2-3点，通过研究这些内容，可以使你达到研究目标
* 可具体到可实施的点，可以包括理论的、开发、实验等多个方面的点

## 3. 研究方案
* 通过流程图的形式表述自己的要通过做什么过程、方法来完成研究内容
* 并简述流程图里的大致内容
* 在此也可以定出时间节点以及需要完成的东西，一般可以遵循以下原则（根据自己的研究选择下面的几个点，不是每个点都必须）

    - 初步调研（搜索、阅读文献、总结文献） （1~2周）
    - 软件是否有参考？找到可以参考借鉴的代码。
    - 系统的流程、架构构思与设计
    - 硬件初步设计（根据**需求**初步设计方案、可以手绘，可以通过solidworks画图）（1周）
    - 市场调研及硬件再设计（根据初步设计，调研市场上是否具有所需要零件，是否自己能做出来所需要的零件等，再依次修正设计方案）（1~2周）
    - 购买、制定、自己动手制作搭建硬件平台（2~3周）
    - 连入电子系统，进行编程、调试等（3~4周）（注意，次条和上条可以根据软硬件的难度、需求合理安排时间）
    - 实验、数据分析（1周）
    - 撰写论文（2~3周）具体可以参考

## 4. 预期成果
* 列出自己的研究成果，如：
    * 论文
    * 发明专利、软件著作权
    * 可演示硬件平台
    * 可演示软件平台等

