# 需要达到的水平

通过一步一步拓展自己的能力范围，逐步达到下述的能力。大家需要思考如何达到下面所列的能力，如何有效评估自己的能力是否达到？


## 1. 目标分析能力
* [ ] 是否会分析课题的目标
* [ ] 能否制定和自身能力适配的目标
* [ ] 能否分解整体目标到阶段目标
* [ ] 是否会制定合理可行的计划
* [ ] 是否会评估自己的能力


## 2. 知识与技能
* [ ] [编程能力是否达标](https://gitee.com/pi-lab/learn_programming/blob/master/Target.md)
* [ ] 能否会用[C++](https://gitee.com/pi-lab/learn_programming)或者[Python](https://gitee.com/pi-lab/machinelearning_notebook/tree/master/0_python)完成程序编写
* [ ] 是否会用PyTorch等框架完成机器学习程序的编写
* [ ] 是否会用OpenCV等库完成计算机视觉程序的编写
* [ ] 其他常用库是否会熟练使用


## 3. 工具
* [ ] 能否熟练使用`Git`管理代码、文档
* [ ] 能否使用`Markdown`来编写文档
* [ ] 是否会用Latex来写论文
* [ ] 是否会用Linux


## 4. 研究、写作能力
* [ ] 是否会找到突破口
* [ ] 是否知道去哪里找文献资料
* [ ] 如何读文献
* [ ] 是否会制定研究计划
* [ ] 如何把论文中的方法复现出来
* [ ] 如何设计实验？如何做实验？
* [ ] 是否知道如何订论文大纲
* [ ] 如何分块完成论文
* [ ] 如何凝练创新点


## 5. 思维、方法论
* [ ] 如何分析问题，梳理项目的整体内容、计算点
* [ ] 分析出关键的技术点
* [ ] 如何去找相关资料
* [ ] 如何将自己做的工作给其他人讲清楚
* [ ] 如何把自己做的工作总结、汇总好
