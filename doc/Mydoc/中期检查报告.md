# 一、论文工作任务与进度安排情况

虽然惯性视觉里程计（*visual*-inertial odometry，VIO）已经能够在位置环境下进行较为精确的无人机定位，但是由于IMU惯性测量单元的噪声以及视觉无法避免的累计误差，使得系统在长时间运行后趋向不稳定，会出现较大的漂移。为此，通常的方式是加入没有累计误差的一个全局定位信息对VIO的结果进行修正。比较常见的是GPS，但是由于其在室内环境定位效果差，我们使用了室内定位更加精确的超宽带（Ultra Wide Band，*UWB*）技术与VIO系统进行融合。

我们的工作主要是设计一个VIO与UWB融合的系统。任务在前期主要集中在单个无人机节点上，在单个无人机上取得较好的效果之后，我们也将UWB信息用于集群无人机的协同定位中去。下表列出了论文的主要工作任务及其完成情况。其中未完成的部分将在第四节详细介绍时间安排情况。

| 序号 | 论文任务                                              | 进度     |
| ---- | ----------------------------------------------------- | -------- |
| 1    | 完成VIO与UWB的融合工作，设计融合系统，进行代码实现    | 已完成   |
| 2    | 对融合系统进行仿真测试，验证系统可行性                | 已完成   |
| 3    | 与现有算法进行比较，评估融合算法性能                  | 正在进行 |
| 4    | 对融合系统进行真机测试，并对可能出现的问题进行改进    | 未完成   |
| 5    | 将UWB信息应用到集群无人机的协同定位中去，完成仿真测试 | 未完成   |
| 6    | 进行论文编写                                          | 未完成   |

下面对已完成的任务进行总结，并介绍正在进行的和未完成的任务的详细内容。

1. 完成VIO与UWB的融合工作，设计融合系统，进行代码实现

   该任务在3月底已经基本完成，下面是实现的具体思路和内容。

   设计思路是UWB系统和VIO系统能够独立对无人机的位置进行观测和计算。下面是两个系统的实现

   - UWB系统的实现

     我们使用8个静止的UWB anchor节点对无人机的位置进行观测。由于UWB节点能够获取各节点之间的相对距离，在UWB anchor节点的位置初始化完成后，我们再将一个UWB 节点放到无人机上，对此节点进行观测。使用Nooploop公司的内部解算机制，我们能够获取关于无人机的定位，其误差在30cm以内。

   - VIO系统的实现

     VIO的代表是于2019年发布的VINS-Mono，是HKUST的Shen Shaojie团队开源的一套Visual-Inertial融合定位算法。我们使用这一开源算法作为整个融合系统的基础。VINS-Mono是一种用于单目视觉——惯性系统的实时SLAM框架。它使用了一种基于优化的滑动窗口公式，以提供高精度的视觉惯性里程计。它具有高效的IMU预集成功能，包括偏置校正、自动估计器初始化、在线外部校准、故障检测和恢复、环路检测、全局位姿图优化、地图合并、位姿图重用、在线时间校准、支持尺度恢复。

   在对两个系统进行融合的时候，我们采用松耦合的方式。由于VINS系统的误差主要来自于长时间的漂移，其短时间内的相对位姿变换能够达到较高的精度；与VINS相反，UWB系统主要来源于对无人机绝对位置的随机噪声误差，并没有累积误差。为此，我们设计了如下的融合方式。

   设$\pmb x_i=  [\pmb p_i,\pmb q_i]$为无人机在$i$时刻的位姿，无人机的全局变量$\mathcal X$为
   $$
   \mathcal  X = [\pmb x_1,\pmb x_2,\cdots,\pmb x_n]
   $$
   设$\hat{\mathbf{z}}_{i}^{V},\hat{\mathbf{z}}_{i}^{U}$分别为VINS和UWB测量的无人机在$i$时刻的位姿值，其中VINS测量的$\hat{\mathbf{z}}_{i}^{V}$包含了无人机的位置变量$ \hat{\pmb p}_i^V$和姿态变量$ \hat{\pmb q}_i^V$，UWB测量的$\hat{\mathbf{z}}_{i}^{V}$只包含无人机的$ \hat{\pmb p}_i^U$。

   下面，给出最终的松耦合的优化方程：
   $$
   \min_{\mathcal{X}} \Bigg\{
   \sum_{i=1 }^{n-1}
   \left\|\mathbf{r}_{\mathcal{V}}(\hat{\mathbf{z}}_i^{V},\hat{\mathbf{z}}_{i+1}^{V}, \mathcal{X})
   \right\|_{\mathbf{P}^{V}}^{2}
   
   +\sum_{i=1 }^{n} \rho \Big(
   \left\|\mathbf{r}_{\mathcal{U}}(\hat{\mathbf{z}}_{i}^{U}, \mathcal{X})
   \right\|_{\mathbf{P}^{U}}^{2}\Big)\Bigg\}
   $$
   其中$\mathbf{r}_{\mathcal{V}}(\hat{\mathbf{z}}_i^{V},\hat{\mathbf{z}}_{i+1}^{V}, \mathcal{X})$和$\mathbf{r}_{\mathcal{U}}(\hat{\mathbf{z}}_{i}, \mathcal{X})$分别是VINS和UWB测量的残差，它们的定义将在下文给出。此外，$\mathbf P^V$和$\mathbf P^U$分别为VINS与UWB残差的权重。

   其中核函数用于防治UWB出现过大的噪声，被定义为
   $$
   \rho(s)= \begin{cases}1 & s \geq 1 \\ 2 \sqrt{s}-1 & s<1\end{cases}
   $$

   - VINS残差

     由于VINS给出的两帧之间的无人机的相对位姿变化是比较精确的，对于$i,i+1$时刻的$\hat{\mathbf{z}}_i^{V},\hat{\mathbf{z}}_{i+1}^{V}$，设它们之间的相对位姿变换为
     $$
     \mathbf T_i^{i+1} = \{\hat{\mathbf{z}}_i^{V}\} {}^{-1} \cdot  \hat{\mathbf{z}}_{i+1}^{V}
     $$
     则
     $$
     \mathbf{r}_{\mathcal{V}}(\hat{\mathbf{z}}_i^{V},\hat{\mathbf{z}}_{i+1}^{V}, \mathcal{X}) = \pmb x_i \mathbf T_i^{i+1} - \pmb x_{i+1}
     $$

   - UWB残差
     $$
     \mathbf{r}_{\mathcal{U}}(\hat{\mathbf{z}}_{i}, \mathcal{X}) = \pmb x_i - \hat{\mathbf{z}}_{i}
     $$

​			至此，我们给出了VIO与UWB融合的数学理论依据。同样，基于VINS-Mono，我们开发了UWB与VIO融合的代码。

2. 对融合系统进行仿真测试，验证系统可行性

   VIO的数据集有很多，我们使用了euroc的数据集进行测试。在此基础上，我们模拟了UWB信号，在$x,y,z$轴方向上，分别在无人机飞行轨迹的真值上加上$\mathcal N(0,0.1)$的噪声模拟UWB数据（即$\sigma=10cm$）。初步验证了系统的可行性。

   下图为UWB测得的带有高斯噪声的轨迹。

   ![uwb_sim_0.1_traj](F:\PIC\uwb_sim_0.1_traj.png)

   下图为UWB测得的带有高斯噪声的ATE值，可以看到偏差在5-15cm居多，能够较好的模拟真实情况。

   ![噪声效果](F:\PIC\噪声效果.png)

   下面是VINS的轨迹与真值的比较。

   ![vins_traj](F:\PIC\vins_traj.png)

   下面是我们的融合算法的轨迹与真值的比较。

   ![uwb_fusion_0.1_traj](F:\PIC\uwb_fusion_0.1_traj.png)

3. 与现有算法进行比较，评估融合算法性能

   这一步我们正在进行之中，暂时只与纯VINS方法进行了比较，分别评价轨迹的ATE-absolute trajectory error值。为了降低UWB系统可能带来的精度，我们在仿真数据中的$\sigma=0.1m$

   
   
   这一步我们正在进行之中，暂时只与纯VINS方法进行了比较，分别评价轨迹的APE-Absolute Pose Error值。它的计算方法如下：
   
   考虑一条估计轨迹$\boldsymbol{T}_{e s t i, i}$和真实轨迹$\boldsymbol{T}_{g t, i}$,其中$i=1, \cdots, N$,则估计轨迹与真值轨迹的绝对轨迹误差为
   $$
   \mathrm{APE}=\operatorname{sqrt}\left(\frac{1}{N} \sum_{i=1}^{N}\left\|\log \left(\boldsymbol{T}_{g t, i}^{-1} \boldsymbol{T}_{e s t i, i}\right)^{\vee}\right\|_{2}^{2}\right)
   $$
   
   
   
   
   | dataset         | VINS     | UWB VIO Fusion($\sigma=0.1$) |
   | --------------- | -------- | ---------------------------- |
   | MH_01_easy      | 0.183636 | 0.00642215                   |
   | MH_02_easy      | 0.190880 | 0.006295                     |
   | MH_03_medium    | 0.401056 | 0.008180                     |
   | MH_04_difficult | 0.386542 | 0.008497                     |
   | MH_05_difficult | 0.372230 | 0.007968                     |

​	下面是MH_01_easy数据集下的算法表现

![2](F:\PIC\2.png)

后续，我们将对不同的$\sigma$，融合系统进行优化求解时UWB和VINS的不同权重值$\mathbf P^V$和$\mathbf P^U$，以及不同的数据集，不同的融合算法进行测试，评估我们的算法。

4. 对融合系统进行真机测试，并对可能出现的问题进行改进

   由于UWB的数据是模拟得来的，真实环境下其性能可能与预期有所偏差。后续我们将进行真机实现，记录相机、IMU和UWB数据，并在我们的系统上进行测试。由于获取真实的UWB数据的接口与仿真有所差距，其数据也可能和理想的高斯分布有较大的差距，所以届时可能还会对算法进行相应调整。

5. 将UWB信息应用到集群无人机的协同定位中去，完成仿真测试

   完成单个无人机的VIO与UWB融合之后，我们还计划完成集群无人机使用UWB进行相对定位的仿真实验。

6. 进行论文编写

# 二、论文相关资料查阅情况

- 论文[1]提出了一个开源的VIO系统，VINS-Mono。VINS-Mono是一种用于单目视觉——惯性系统的实时SLAM框架。它使用了一种基于优化的滑动窗口公式，以提供高精度的视觉惯性里程计。它具有高效的IMU预集成功能，包括偏置校正、自动估计器初始化、在线外部校准、故障检测和恢复、环路检测、全局位姿图优化、地图合并、位姿图重用、在线时间校准、支持尺度恢复。VINS-Mono主要用于自动驾驶无人机的状态估计和反馈控制，但它也可以用于自动驾驶无人机。
- 论文[2]提出了一种使用VIO+单个UWB Anchor的融合系统VIR，能够以一种紧耦合的方式对无人机的姿态进行修正。通过对位置UWB Anchor节点进行初始化，能够估计出Anchor节点在无人机VIO坐标系下的位置。得到初始位置后，以后每个时刻都可以接收到无人机与Anchor节点的距离，这样相当于无人机的视觉有了一个固定的路标点，且每一帧都可以观测到。通过这样的方式能够较好的提升纯VIO系统的精度。
- 论文[3]同样是基于VIO+单个UWB节点的融合系统，不同的是它在论文[2]的基础上改进了UWB数据的利用率，通过使用所有时刻的UWB数据，提升了系统的信息利用率，最好效果也得到了显著提升。
- 论文[4]提出了集群无人机使用UWB进行相对定位的方式，将UWB测量的无人机之间的相对距离加入到最终的优化方程中去。
- 论文[5]同本系统一样，也是使用了8个UWB Anchor节点对无人机进行定位，不同的是融合的对象是单目视觉SLAM，并没有IMU的加入。

# 三、存在问题及采取的措施

- 融合系统中VINS与UWB残差的权重：$\mathbf P^V$和$\mathbf P^U$对现有系统的表现有较为关键的影响因素，后续需通过实验找出最优值，最好能找出规律。
- 进行真机实现时，无人机的轨迹真值无法获取。由于缺乏较为昂贵的动态捕捉设备，我们计划采用较为简单的实验方式进行真机实验。
  - 可以通过走一个半径为5m的圆，通过把UWB采集的关于无人机的位置强制复位到圆环上，用于地面真值。
  - 可以通过走特定的字母，如$M$来可视化的观察系统得出的轨迹的效果。
- 现在的算法框架下，在无人机处于静止状态时，由于VINS测得的相对变化关系较小，UWB测到的无人机的变化很大很剧烈，优化公式中VIO的约束能力将下降。可以后续通过自动检测无人机的这种异常情况，赋予VINS与UWB残差动态权重，优化系统的表现。

# 四、下一阶段论文工作安排

- 进行真机实验，并根据真实数据对现有系统的效果进行修正和评价，进一步提升融合系统的实际性能.
- 进一步提升算法性能，比如根据UWB和VIO的数据动态的设置优化函数中UWB和VINS残差的权重.
- 对算法中的不同参数进行测试，发现规律并用于提升算法的依据.
- 完成集群无人机协同定位的仿真测试.
- 编写最终的毕业论文.

# 五、参考文献

[1] Tong Q ,  Li P ,  Shen S . VINS-Mono: A Robust and Versatile Monocular Visual-Inertial State Estimator[J]. IEEE Transactions on Robotics, 2017, PP(99):1-17.

[2] Cao Y ,  Beltrame G . VIR-SLAM: Visual, Inertial, and Ranging SLAM for single and multi-robot systems[J]. arXiv, 2020.

[3] Nguyen T H ,  Nguyen T M ,  Xie L . Range-focused Fusion of Camera-IMU-UWB for Accurate and Drift-reduced Localization[J]. IEEE Robotics and Automation Letters, 2021, PP(99):1-1.

[4] Xu H ,  Wang L ,  Y  Zhang, et al. Decentralized Visual-Inertial-UWB Fusion for Relative State Estimation of Aerial Swarm[J].  2020.

[5] Tiemann J ,  Ramsey A ,  Wietfeld C . Enhanced UAV Indoor Navigation through SLAM-Augmented UWB Localization[C]// 2018 IEEE International Conference on Communications Workshops (ICC Workshops). IEEE, 2018.

